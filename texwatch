#!/bin/bash

# Defaults
build_systems=(single make)
build_system=single
maketarget=all
makeflags=()
viewer=mupdf
timeout=10 #secs
texcompiler=pdflatex
hasher=md5sum
notification_timeout=4000 #ms
verbosity=0
verify_checksum=0


tmpdir=/tmp/texwatch/$$
mkdir -p $tmpdir/{cache,build}

buildlog=$tmpdir/build/log
cachedir=$tmpdir/cache
view_file=$cachedir/live.pdf
hashdir=$cachedir/hashes

mkdir $hashdir

trap cleanup EXIT

function cleanup {
    [ -d $tmpdir ] && rm -rf "$tmpdir"
    (( $(ls $(dirname "$tmpdir") | wc -l) )) || \
        rm -rf $(dirname "$tmpdir")
}

function verbose {
    (( $verbose )) && printf "$*" >&2
}

function format_array {
    echo "$*" | sed 's/ /, /g' | sed -E 's/(^.*),([^,].*$)/\1 and \2/g'
}

function usage {
    local fd=$([ -z "$1" ] && echo 1 || echo "$1")
    cat <<- EOF >&$fd
		texwatch [OPTION]... FILE
		    -b SYSTEM, --build-system=SYSTEM        Set build system, options are
	 	                                            $(format_array "${build_systems[@]}").
		    -m TARGET, --make-target=TARGET         Specify make target, avaiable only with
		                                            build system set to make
		    -f FLAG[,...], --make-flags=FLAG[,...]  Specify flags to pass to make, available
		                                            only with build system make
		    -w VIEWER, --viewer=VIEWER              Set pdf viewer, default is $viewer
		    -t TIMEOUT, --timeout=TIMEOUT           Set the timeout for compilation
		    -c, --verify-checksum                   Verify that the monitored file truly has changed
		                                            before compiling. Useful for large projects
		    -v, --verbose                           Verbose output
		    -h, --help                              Print usage
	EOF
}


####################
# Dependency checks
####################

# Inotify
function check_inotify_available {
    command -v inotifywait >/dev/null || { echo "inotify-tools required" >&2; exit 1; }
}

# Libnotify
function check_libnotify_available {
    notify_available=$(( !$(command -v notify-send >/dev/null; echo $?) ))
    (( !$notify_available )) && echo Warning: libnotify not installed, no notifications available >&2
}

# Pdf viewer
function check_viewer_available {
    command -v "$viewer" >/dev/null || { echo "$viewer is not installed" >&2; exit 1; }
}

function check_main_file_exists {
    [ -z $main_file ] && echo "No file specified" >&2 && exit 1
    ! [ -f $main_file ] && echo "File $main_file does not exist" >&2 && exit 1
}

#####################
# Switch verification
#####################

function require_nonempty {
    [ -z "$1" ] && shift && echo "$*" >&2 && exit 1
}

function require_numeric {
    echo "$1" | grep -qP "^\d+(\.\d+)?$" || { shift && echo "$*" >&2 && exit 1; }
}

function verify_build_system {
    if ! echo ${build_systems[@]} | grep -q "$(echo $build_system | tr -d ' ')"  ; then
         echo "$build_system is not an available build system" >&2
         exit 1
    fi
}

function verify_makeflag {
    if echo "$*" | grep -qvE "(^-[a-zA-Z].*)|(^--[a-zA-Z]{2}.*)" ||
       ! man make | grep -q $(echo "$*" | sed 's/-/\\-/g') ; then
         echo "$*" is not a make flag >&2 && exit 1
    fi
}

# Verify flags passed to make
function append_makeflags {
    local make_man=$(man make)
    while read -r flag ; do
        verify_makeflag $flag
        makeflags=("${makeflags[@]}" $flag)
    done < <(echo "$*" | tr ',' '\n')
}

####################
# Main functionality
####################

# Get name of Pdf file
function pdf_name {
    echo $(echo $main_file | sed -E 's/\.[^.]*$/\.pdf/g')
}

# Compare hashes
function file_updated {
    if (( $verify_checksum == 0 )) ; then
        verbose "Skipping checksum check\n"
        # Don't verify, assume change always occurs
        echo 1
        return
    fi

    local filename="$1"
    local cached_name=$(echo "$filename" | tr '/' '#' | sed -E 's/\.[^.]+$/\.hash/')
    local hash_file=$hashdir/$cached_name
    local current_hash=$(eval $hasher "$filename" | cut -d ' ' -f 1)

    if ! [ -f $hash_file ] ; then
        verbose "Hash of $filename not in cache, reloading\n"
        # Overwrite hash
        echo $current_hash > $hash_file
        # File has not been hashed yet, don't know previous checksum
        echo 1
        return
    fi

    local prev_hash=$(<$hash_file)

    if [ $current_hash == $prev_hash ] ; then
        verbose "File $filename unchanged, skipping compilation\n"
        echo 0
    else
        verbose "Hash of $filename changed, recompiling\n"
        echo $current_hash > $hash_file
        echo 1
    fi
}

# Send notification on failure
function notify {
    (( $notify_available )) || return
    notify-send -t $notification_timeout --hint int:transient:1 --icon=dialog-error \
        "Compilation of $(basename $main_file) failed" "See $buildlog for output"
}

# Build pdf
function build {
    verbose Building...
    if [ $build_system == make ] ; then
        timeout $timeout make $makeflags $maketarget > $buildlog 2>&1
    else
        timeout $timeout $texcompiler $main_file > $buildlog 2>&1
    fi
    local status=$?
    if (( $status )) ; then
        verbose "Failed\n"
        notify
        cat $buildlog >&2
    else
        cp $(pdf_name) $view_file
        verbose "Ok\n"
    fi
    echo $status
}

# Get pid of viewer
function viewer_pid {
    local pids=$(pgrep "$viewer")
    while read -r pid ; do
        if ps $pid | grep -qE "$viewer.*$view_file" ; then
            echo $pid
            break
        fi
    done <<< "$pids"
}

# Reload viewer
function update_viewer {
    local pid=$(viewer_pid)
    if [ -z "$pid" ] ; then
        verbose "Launching $viewer\n"
        $viewer $view_file &
    else
        verbose "Updating viewer with pid $pid\n"
        kill -HUP "$pid"
    fi
}

# Main loop
function run_daemon {
    local dir=$(dirname $(readlink -f $main_file))
    local status=0
    cd  $dir
    while : ; do
        modified_file=$(inotifywait -qre modify --format '%w%f' $dir)
        if (( $(file_updated $modified_file) )) ; then
            status=$(build)
        fi
        (( $status == 0 )) && update_viewer
    done
}

################
# Switch parsing
################

eval set -- $(getopt -o "b:m:f:w:t:cvh" -l "build-system:,make-target:,make-flags:,viewer:,timeout:,verify-checksum,verbose,help" -n "texwatch" -- "$@")

while ! [ -z "$1" ] ; do
    case "$1" in
        -b | --build-system )
            build_system="$2"
            shift 2
        ;;
        -m | --make-target )
            maketarget="$2"
            shift 2
        ;;
        -f | --make-flags )
            append_makeflags "$2"
            shift 2
        ;;
        -w | --viewer )
            viewer="$2"
            shift 2
        ;;
        -t | --timeout )
            require_numeric "$2" "Argument to $1 must be numeric"
            timeout="$2"
            shift 2
        ;;
        -c | --verify-checksum )
            verify_checksum=1
            shift
        ;;
        -v | --verbose )
            verbose=1
            shift
        ;;
        -h | --help )
            usage 1
            exit 0
        ;;
        -- )
            require_nonempty "$2" "No file specified"
            main_file="$2"
            shift
            break
        ;;
        * )
            main_file="$1"
            shift
        ;;
    esac
done

check_inotify_available
check_libnotify_available
check_viewer_available
check_main_file_exists

verify_build_system

run_daemon
