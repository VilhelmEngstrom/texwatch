# texwatch
A script for automatically reloading latex-generated PDFs.   

Supports both compiling single latex files with a tex compiler and make for building larger projects. Modifications to any file in, or in a subdirectory of, the directory of the file passed to texwatch is monitored via inotify.

The PDF is assumed to have the same stem as the supplied file. In other words, if running `texwatch [OPTION]... foo.tex`, the script assumes the generated PDF is called `foo.pdf`. See `texwatch -h` for a list of available options.

# Dependencies
- GNU getopt
- inotify-tools
- libnotify (for notifications, optional)
- A [notification server](https://wiki.archlinux.org/index.php/Desktop_notifications#Notification_servers) (for notifications, optional)
- pdflatex (default tex compiler, change the `texcompiler` variable to use another)
- mupdf (default PDF viewer, change the `viewer` variable to use another)
